# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import CaseRelationInformation, get_case, get_user
from zsnl_domains.database.schema import Logging


class CaseRelationBase(BaseLogger):
    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """

        formatted_entity_data = self.format_entity_data(event=event)

        case_uuid = formatted_entity_data["current_case_uuid"]
        case_info = get_case(session=session, uuid=case_uuid)

        user_info = get_user(session=session, uuid=event["user_uuid"])

        if event["event_name"] == "CaseRelationDeleted":
            case_relation_info = CaseRelationInformation(
                case_id_a=formatted_entity_data["case_id_a"],
                case_id_b=formatted_entity_data["case_id_b"],
            )

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            case_id=case_info.id,
            case_relation_info=case_relation_info,
        )
        event_parameters["correlation_id"] = event["correlation_id"]

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=case_info.id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=(case_info.confidentiality != "public"),
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        case_id: int,
        case_relation_info: CaseRelationInformation,
    ):
        raise NotImplementedError


class CaseRelationDeleted(CaseRelationBase):
    component = "zaak"
    subject = "Relatie met zaak {} verwijderd"
    variables = ["relation_id"]
    event_type = "case/relation/remove"

    def generate_event_parameters(
        self,
        entity_data: dict,
        case_id: int,
        case_relation_info: CaseRelationInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "relation_id": case_relation_info.case_id_b
            if case_relation_info.case_id_a == case_id
            else case_relation_info.case_id_a,
        }

        return event_parameters
