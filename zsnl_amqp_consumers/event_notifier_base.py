# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .repository import UserInformation
from abc import ABCMeta, abstractmethod
from minty import Base
from zsnl_domains.database.schema import Message


class BaseNotifier(Base, metaclass=ABCMeta):
    @abstractmethod
    def __call__(self):
        pass

    def _create_message_notifier_record(
        self,
        assignee_info: UserInformation,
        subject: str,
        logging_id: int,
        is_read=False,
        is_archived=False,
    ) -> Message:
        """Create `Message` record from event data.

        :param user_info: information about user
        :type user_info: UserInformation
        :param subject: message logline
        :type subject: str
        :param logging_id_id: id of entry in logging table
        :type logging_id_id: int
        :param is_read: if the message is read
        :type is_read: bool
        :param is_archived: if the message is read
        :type is_archived: bool
        :return: Message record
        : rtype: Message"""

        message = Message()
        message.logging_id = logging_id
        message.message = subject
        message.subject_id = f"betrokkene-medewerker-{assignee_info.id}"
        message.is_read = is_read
        message.is_archived = is_archived
        return message

    def format_entity_data(self, event) -> dict:
        """Create formatted dict from event.changes & event.entity_data.

        :param event: event
        :type event: event
        :return: dict with entity_data
        :rtype: dict
        """
        formatted_event_data = dict(event["entity_data"])
        for change in event["changes"]:
            formatted_event_data[change["key"]] = change["new_value"]
            old_key = f"{change['key']}_old"
            formatted_event_data[old_key] = change["old_value"]
        return formatted_event_data
