# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_amqp_consumers import event_logger_cm_task, repository


class TestEventLoggerCommunication:
    def setup(self):
        self.task_logger = event_logger_cm_task.TaskLogger()
        self.session = mock.MagicMock()

    @mock.patch("zsnl_amqp_consumers.event_logger_cm_task.repository")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_cm_task.TaskLogger._create_logging_record"
    )
    def test_call(self, _create_logging_record, mock_repository):
        event = {
            "changes": [
                {
                    "key": "case",
                    "new_value": {
                        "id": 448,
                        "milestone": 1,
                        "status": "open",
                        "uuid": "16d82ad6-e7e8-4733-8512-940edd2a0b8b",
                    },
                    "old_value": {
                        "entity_id": "16d82ad6-e7e8-4733-8512-940edd2a0b8b",
                        "type": "Case",
                    },
                },
                {"key": "title", "new_value": "task title", "old_value": None},
                {"key": "phase", "new_value": 2, "old_value": None},
                {"key": "completed", "new_value": False, "old_value": None},
                {"key": "user_defined", "new_value": True, "old_value": None},
                {"key": "description", "new_value": "", "old_value": None},
            ],
            "context": "localhost:6543",
            "correlation_id": "aa8f6f7c-f302-4e2b-8eb0-22cd94245561",
            "created_date": "2019-12-20T09:23:27.728098",
            "domain": "zsnl_domains.case_management",
            "entity_data": {
                "case": {
                    "id": 448,
                    "milestone": 1,
                    "status": "open",
                    "uuid": "16d82ad6-e7e8-4733-8512-940edd2a0b8b",
                },
                "title": "task title",
            },
            "entity_id": "46d82ad7-e7e8-4743-8514-940edd4a0b8a",
            "entity_type": "Task",
            "event_name": "TaskCreated",
            "id": "d31bfc2d-e9c0-40b7-a04f-12c20631a70d",
            "user_uuid": "8a16c255-5152-48b1-8775-5c86044c5158",
        }
        case = repository.CaseInformation(
            uuid=uuid4(), id=448, confidentiality="public"
        )
        mock_repository.get_case.return_value = case
        user_info = repository.UserInformation(
            id=123, display_name="firstname lastname", type="medewerker"
        )
        mock_repository.get_user.return_value = user_info
        self.task_logger(session=self.session, event=event)

        _create_logging_record.assert_called_with(
            user_info=user_info,
            event_type="case/task/created",
            subject="Taak 'task title' aangemaakt.",
            component="zaak",
            component_id=None,
            case_id=448,
            created_date="2019-12-20T09:23:27.728098",
            event_data={
                "uuid": "46d82ad7-e7e8-4743-8514-940edd4a0b8a",
                "title": "task title",
                "correlation_id": "aa8f6f7c-f302-4e2b-8eb0-22cd94245561",
            },
            restricted=False,
        )

    def test_generate_task_info(self):
        title = "some task title"
        event = "TaskCreated"
        subject = f"Taak '{title}' aangemaakt."
        type = "case/task/created"

        res = self.task_logger.generate_task_info(
            title=title, event_name=event
        )
        assert res.type == type
        assert res.subject == subject

        event = "TaskDeleted"
        subject = f"Taak '{title}' verwijderd."
        type = "case/task/deleted"

        res = self.task_logger.generate_task_info(
            title=title, event_name=event
        )
        assert res.type == type
        assert res.subject == subject

        event = "TaskUpdated"
        subject = f"Taak '{title}' gewijzigd."
        type = "case/task/updated"
        res = self.task_logger.generate_task_info(
            title=title, event_name=event
        )
        assert res.type == type
        assert res.subject == subject
