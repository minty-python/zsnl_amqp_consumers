# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_amqp_consumers import (
    event_notifier_communication,
    event_notifier_document,
    repository,
)


class TestEventNotifierDocument:
    def test_case_base_generate_subject(self):
        base = event_notifier_document.DocumentBase()
        with pytest.raises(NotImplementedError):
            base._generate_subject(document_info=None)

    @mock.patch("zsnl_amqp_consumers.event_notifier_document.get_document")
    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_document.get_case_assignee"
    )
    def test_document_added_to_case_event(
        self, mock_case_assignee, mock_document
    ):
        document_event = event_notifier_document.DocumentAddedToCase()
        session = mock.MagicMock()
        case_uuid = uuid4()
        user_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "description",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "origin", "old_value": None, "new_value": "Inkomend"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(user_uuid),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAddedToCase",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_document.return_value = repository.DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )
        mock_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=uuid4()
        )

        message = document_event(session=session, event=event, logging_id=13)

        assert message.logging_id == 13
        assert message.message == "Document 'test_doc' assign"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

        # When document cannot be found, no message notification.
        mock_document.side_effect = NotFound
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None

        # When case assignee is the logged in user, no need of message notification
        mock_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=user_uuid
        )
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None

        # When case has no assignee, no need of message notification
        mock_case_assignee.side_effect = NotFound
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None

        # When document is not a case_document, no need of message notification
        del event["changes"][3]
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None

    @mock.patch("zsnl_amqp_consumers.event_notifier_document.get_document")
    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_document.get_case_assignee"
    )
    def test_document_created_event(self, mock_case_assignee, mock_document):
        document_event = event_notifier_document.DocumentCreated()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "basename",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "extension", "old_value": None, "new_value": ".pdf"},
                {"key": "store_uuid", "old_value": None, "new_value": uuid4()},
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_document.return_value = repository.DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )
        mock_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=uuid4()
        )

        message = document_event(session=session, event=event, logging_id=13)

        assert message.logging_id == 13
        assert message.message == "Document 'test_doc' aangemaakt"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False


class TestEventNotifierCommunication:
    def test_case_base_generate_subject(self):
        base = event_notifier_communication.CommunicationBase()
        with pytest.raises(NotImplementedError):
            base._generate_subject(entity_data=None)

    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_assignee"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_from_thread"
    )
    def test_external_message_created_event(
        self, mock_get_case_from_thread, mock_get_case_assignee
    ):
        communication_event = (
            event_notifier_communication.ExternalMessageCreated()
        )
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_assignee_uuid = uuid4()
        event = {
            "changes": [
                {"key": "store_uuid", "old_value": None, "new_value": uuid4()},
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "pip",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case_from_thread.return_value = repository.CaseInformation(
            id=12, confidentiality="public", uuid=uuid4()
        )
        mock_get_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=case_assignee_uuid
        )

        message = communication_event(
            session=session, event=event, logging_id=15
        )

        assert message.logging_id == 15
        assert message.message == "PIP-bericht toegevoegd"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

        # when logged in user is same as that of case_assignee, no notification
        event["user_uuid"] = str(case_assignee_uuid)
        message = communication_event(
            session=session, event=event, logging_id=15
        )
        assert message is None

        # when logged in user is a pip user, create notification
        event["changes"].append(
            {"key": "creator_type", "old_value": None, "new_value": "pip"}
        )
        message = communication_event(
            session=session, event=event, logging_id=15
        )
        assert message.logging_id == 15
        assert message.message == "PIP-bericht toegevoegd"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

        # case is not assigned, no message notification
        mock_get_case_assignee.side_effect = NotFound
        message = communication_event(
            session=session, event=event, logging_id=15
        )
        assert message is None

        # when pip message is not associated to a case, no message notification
        mock_get_case_from_thread.side_effect = NotFound
        message = communication_event(
            session=session, event=event, logging_id=15
        )
        assert message is None

    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_assignee"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_from_thread"
    )
    def test_note_created_event(
        self, mock_get_case_from_thread, mock_get_case_assignee
    ):
        communication_event = event_notifier_communication.NoteCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        event = {
            "changes": [
                {"key": "store_uuid", "old_value": None, "new_value": uuid4()},
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "creator_type",
                    "old_value": None,
                    "new_value": "employee",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "Note",
            "event_name": "NoteCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case_from_thread.return_value = repository.CaseInformation(
            id=12, confidentiality="public", uuid=uuid4()
        )
        mock_get_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=uuid4()
        )

        message = communication_event(
            session=session, event=event, logging_id=15
        )

        assert message.logging_id == 15
        assert message.message == "Notitie toegevoegd"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_assignee"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_from_thread"
    )
    def test_contact_moment_created_event(
        self, mock_get_case_from_thread, mock_get_case_assignee
    ):
        communication_event = (
            event_notifier_communication.ContactMomentCreated()
        )
        session = mock.MagicMock()
        thread_uuid = uuid4()
        event = {
            "changes": [
                {"key": "store_uuid", "old_value": None, "new_value": uuid4()},
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "creator_type",
                    "old_value": None,
                    "new_value": "employee",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ContactMoment",
            "event_name": "ContactMomentCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case_from_thread.return_value = repository.CaseInformation(
            id=12, confidentiality="public", uuid=uuid4()
        )
        mock_get_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=uuid4()
        )

        message = communication_event(
            session=session, event=event, logging_id=15
        )

        assert message.logging_id == 15
        assert message.message == "Contactmoment toegevoegd"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_assignee"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_notifier_communication.repository.get_case_from_thread"
    )
    def test_email_imported_event(
        self, mock_get_case_from_thread, mock_get_case_assignee
    ):
        communication_event = (
            event_notifier_communication.ExternalMessageCreated()
        )
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_assignee_uuid = uuid4()
        event = {
            "changes": [
                {"key": "store_uuid", "old_value": None, "new_value": uuid4()},
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "email",
                },
                {"key": "is_imported", "old_value": None, "new_value": True},
                {
                    "key": "subject",
                    "new_value": "Test Message",
                    "old_value": None,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case_from_thread.return_value = repository.CaseInformation(
            id=12, confidentiality="public", uuid=uuid4()
        )
        mock_get_case_assignee.return_value = repository.SubjectInformation(
            id=15, display_name="test admin", uuid=case_assignee_uuid
        )

        message = communication_event(
            session=session, event=event, logging_id=15
        )

        assert message.logging_id == 15
        assert message.message == "E-mail toegevoegd"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

    # assert message is None

    @mock.patch("zsnl_amqp_consumers.event_notifier_document.get_document")
    @mock.patch("zsnl_amqp_consumers.event_notifier_document.get_user")
    def test_document_assigned_to_user_event(
        self, mock_get_assignee, mock_get_document
    ):
        document_event = event_notifier_document.DocumentAssignedToUser()
        session = mock.MagicMock()
        assignee_uuid = uuid4()

        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": assignee_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToUser",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = repository.DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )
        mock_get_assignee.return_value = repository.UserInformation(
            id=15, display_name="test admin", type="employee"
        )

        message = document_event(session=session, event=event, logging_id=13)

        assert message.logging_id == 13
        assert message.message == "Document 'test_doc' is aan u toegewezen"
        assert message.subject_id == "betrokkene-medewerker-15"
        assert message.is_read is False
        assert message.is_archived is False

        # When the assignee of document cannot be found
        mock_get_assignee.side_effect = NotFound
        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": assignee_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToUser",
            "id": str(uuid4()),
            "entity_data": {},
        }
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None

        # When the event assignee of document is the logged in user
        # no message notification
        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": assignee_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(assignee_uuid),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToUser",
            "id": str(uuid4()),
            "entity_data": {},
        }
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None

        # When the event 'DocumentAssignedToUser' doesnot have 'intake_owner_uuid'
        # in the changes, no message notification
        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToUser",
            "id": str(uuid4()),
            "entity_data": {},
        }
        message = document_event(session=session, event=event, logging_id=13)
        assert message is None
